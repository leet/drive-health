#!/bin/bash
#
# Tools to test drive health
#

# ToDo:
#  Check if device is mounted
#  Log all scripting output
#  Check return status of reports and pause/stop

# Enable SMART?
# Enaebl Auto Offline Data Collection?

# Need to be root or sudo

# lsblk

smartctl -H $1

# Check for logging errors
smartctl -l error $1

smartctl -i $1

# Enable SMART
smartctl -s on $1
# automatic off-line testing
smartctl -o on $1

# Check what supported testing can be done and how long
# Pull out and report times
smartctl -c $1

# Collect Attribs
smartctl -a $1
smartctl -x $1

# short
#smartctl -t short $1
# long
#smartctl -t long $1
# offline
#smartctl -t offline $1
# conveyance
#smartctl -t conveyance $1

# Can issue health check after each test
smartctl -H $1

smartctl -l selftest $1

#badblocks -v $1 > badsectors.txt

# This will typically update the "current pending sector" count in SMART.
#badblocks -b 4096 -c 1024 -s $1
